var gulp = require('gulp');
// Requires the gulp-sass plugin
var postcss = require('gulp-postcss');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var browserSync = require('browser-sync').create();


gulp.task('sass', function () {
  return gulp
    .src('src/scss/**/*.scss')
    .pipe(sass({
        outputStyle: 'compact'
    }).on('error', sass.logError))
    .pipe(concat('style.css'))
    .pipe(gulp.dest('./')); // output to theme root
});

gulp.task('js', function () {
  return gulp
    .src(['node_modules/bootstrap/dist/js/bootstrap.bundle.min.js',
    'node_modules/jquery/dist/jquery.min.js',
    'node_modules/tether/dist/js/tether.min.js',
    'node_modules/bootstrap/js/dist/collapse.js',
    'node_modules/datatables.net/js/jquery.dataTables.min.js',
    'node_modules/datatables.net-dt/js/dataTables.dataTables.min.js'])
    .pipe(gulp.dest('src/js'));
});

gulp.task('icons', function () {
  return gulp
    .src(['node_modules/bootstrap-icons/bootstrap-icons.svg'])
    .pipe(gulp.dest('src/img/icons'));
});

gulp.task('watch', function(){
  browserSync.init({
      port: 8081,
      proxy: {
      target: "http://cognassist/",
      }
  });
  gulp.watch('src/scss/**/*.scss', gulp.series('sass'));
  gulp.watch('src.js').on('change', browserSync.reload);
  gulp.watch('./style.css').on('change', browserSync.reload);
  gulp.watch('./*.html').on('change', browserSync.reload);
  // Other watchers
})
