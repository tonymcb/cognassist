$(document).ready(function() {
    $('#example').DataTable();

    $("#nav-icon").click(function(e) {
        $('#nav-icon').toggleClass('active');
        $('html').toggleClass('fix');
        $('#sidenav').toggleClass('active');
        $('#sidenav').fadeToggle();
        $('#header .search').fadeToggle();
        e.preventDefault();
     });

    $("#site-search").on("click", function(event){
      $(".search").addClass('active');
      $("#header").addClass('active');
      $('#site-search').focus();
      event.stopPropagation();
    });

    $(".search .clear").on("click", function(event)
    {
      $('.search').removeClass('active');
      $("#header").removeClass('active');
      $('#site-search').val("");
      event.stopPropagation();
    });

    $(document).on("click", function(event)
    {
      $('.search').removeClass('active');
      $("#header").removeClass('active');
      $('#site-search').blur();
      $('#site-search').val("");
    });

} );
